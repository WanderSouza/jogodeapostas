import telebot
import random
import datetime
from emoji import emojize

numeroAposta = 0
numeroBicho = 0
now = datetime.datetime.now()
token='522900156:AAHAt-tiI1SjgcEokmPZilZkTi8ifGa0Csk'
bot=telebot.TeleBot(token)
wlcm_msg = "Bem-vindo ao jogo do BICHO.\n\nComandos disponiveis:\n/apostar Para realizar uma nova aposta \n/historico Para visualizar suas apostas anteriores \n /resultado Para exibir o resultado atual e conferir se o seu ultimo jogo"

@bot.message_handler(commands=['start'])
def sayhello(message):
    bot.reply_to(message,wlcm_msg) 
    
@bot.message_handler(commands=['apostar'])
def criar_aposta(message):
    words = message.text.split()
    if len(words) == 2:
        aposta = words[1]
        print(aposta) 
        apostaArquivo = open("aposta", "a")
        apostaArquivo.write(aposta+ "\n")
        apostaArquivo.close()
        apostaQuebrada = map(str, str(aposta))
        print(apostaQuebrada)
        numeroAposta = int(apostaQuebrada[0]+apostaQuebrada[1])
        numeroBicho = int(apostaQuebrada[2]+apostaQuebrada[3])
        print(numeroAposta)
        print(numeroBicho)
        apostaAtual = int(aposta)
        bot.reply_to(message, "O numero da sua aposta e "+aposta+"\n Pague o seu boleto em: https://www.boletobancario.com/boletofacil/img/boleto-facil-exemplo.pdf")
    else:
        bot.reply_to(message,"Insira um numero de 4 digitos")
    # print(apostaAtual)
    

@bot.message_handler(commands=['historico'])
def listar_evento(message):
    events = open("aposta", "r")
    reply = events.read()
    bot.reply_to(message,"As suas apostas anteriores:\n"+reply)

@bot.message_handler(commands=['resultado'])
def ver_resultado(message):
    resultadoNumero = random.randint(0,99)
    resultadoAnimal = random.randint(0,99)
    animalPic = ""
    animalName = ""
    if(1<=resultadoAnimal<=4):
        animalPic = ":baby_chick:"
        animalName = "Avestruz"
    if(5<=resultadoAnimal<=8):
        animalPic = ":hatched_chick:"
        animalName = "Aguia"
    if(9<=resultadoAnimal<=12):
        animalPic = ":horse:"
        animalName = "Burro"
    if(13<=resultadoAnimal<=16):
        animalPic = ":bug:"
        animalName = "Borboleta"
    if(17<=resultadoAnimal<=20):
        animalPic = ":dog:"
        animalName = "Cachorro"
    if(21<=resultadoAnimal<=24):
        animalPic = ":goat:"
        animalName = "Cabra"
    if(25<=resultadoAnimal<=28):
        animalPic = ":ram:"
        animalName = "Carneiro"
    if(29<=resultadoAnimal<=32):
        animalPic = ":camel:"
        animalName = "Camelo"
    if(33<=resultadoAnimal<=36):
        animalPic = ":snake:"
        animalName = "Cobra"
    if(37<=resultadoAnimal<=40):
        animalPic = ":rabbit2:"
        animalName = "Coelho"
    if(41<=resultadoAnimal<=44):
        animalPic = ":racehorse:"
        animalName = "Cavalo"
    if(45<=resultadoAnimal<=48):
        animalPic = ":elephant:"
        animalName = "Elefante"
    if(49<=resultadoAnimal<=52):
        animalPic = ":rooster:"
        animalName = "Galo"
    if(53<=resultadoAnimal<=56):
        animalPic = ":cat:"
        animalName = "Gato"
    if(57<=resultadoAnimal<=60):
        animalPic = ":crocodile:"
        animalName = "Jacare"
    if(61<=resultadoAnimal<=64):
        animalPic = ":cat2:"
        animalName = "Leao"
    if(65<=resultadoAnimal<=68):
        animalPic = ":monkey_face:"
        animalName = "Macaco"
    if(69<=resultadoAnimal<=72):
        animalPic = ":pig:"
        animalName = "Porco"
    if(73<=resultadoAnimal<=76):
        animalPic = ":bird:"
        animalName = "Pavao"
    if(77<=resultadoAnimal<=80):
        animalPic = ":turkey:"
        animalName = "Peru"
    if(81<=resultadoAnimal<=84):
        animalPic = ":ox:"
        animalName = "Touro"
    if(85<=resultadoAnimal<=88):
        animalPic = ":tiger:"
        animalName = "Tigre"
    if(89<=resultadoAnimal<=92):
        animalPic = ":bear:"
        animalName = "Urso"
    if(93<=resultadoAnimal<=96):
        animalPic = ":horse:"
        animalName = "Veado"
    if(97<=resultadoAnimal<=99 or resultadoAnimal==0):
        animalPic = ":cow:"
        animalName = "Vaca"

    if(numeroAposta == resultadoNumero and numeroBicho == resultadoAnimal):
        bot.reply_to(message, "O resultado atual e "+str(resultadoNumero)+" "+emojize(animalPic, use_aliases=True)+"("+animalName+")"+"\n\nVoce Ganhou!!!\n Va pegar o seu premio na banquinha mais proxima")
    else:
        bot.reply_to(message, "O resultado atual e "+str(resultadoNumero)+" "+emojize(animalPic, use_aliases=True)+"("+animalName+")"+"\n\nVoce Perdeu :(\nNao foi dessa vez, mas nao desista.\nAposte novamente")


bot.polling()